/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 06/02/2012
 Copyright   : Jeffrey Katcher under contract to Network Appliance.
 Description : Modificação do Postmark para uma versão aging free
 ============================================================================
 */

#include "postmark.h"

/* converts integer values to byte/kilobyte/megabyte strings */
char *scale(i)
int i;
{
   static char buffer[MAX_LINE]; /* storage for current conversion */

   if (i/MEGABYTE)
      sprintf(buffer,"%.2f megabytes",(float)i/MEGABYTE);
   else
      if (i/KILOBYTE)
         sprintf(buffer,"%.2f kilobytes",(float)i/KILOBYTE);
      else
         sprintf(buffer,"%d bytes",i);

   return(buffer);
}

/* converts float values to byte/kilobyte/megabyte strings */
char *scalef(i)
float i;
{
   static char buffer[MAX_LINE]; /* storage for current conversion */

   if (i/(float)MEGABYTE>1)
      sprintf(buffer,"%.2f megabytes",i/(float)MEGABYTE);
   else
      if (i/(float)KILOBYTE)
         sprintf(buffer,"%.2f kilobytes",i/(float)KILOBYTE);
      else
         sprintf(buffer,"%f bytes",i);

   return(buffer);
}

/* UI callback for 'set size' command - sets range of file sizes */
int cli_set_size(param)
char *param; /* remainder of command line */
{
   char *token;
   int size;

   if (param && (size=atoi(param))>0)
      {
      file_size_low=size;
      if ((token=strchr(param,' ')) && (size=atoi(token))>0 &&
         size>=file_size_low)
         file_size_high=size;
      else
         file_size_high=file_size_low;
      }
   else
      fprintf(stderr,"Error: no file size low or high bounds specified\n");

   return(1);
}

/* UI callback for 'set number' command - sets number of files to create */
int cli_set_number(param)
char *param; /* remainder of command line */
{
   int size;

   if (param && (size=atoi(param))>0)
      simultaneous=size;
   else
      fprintf(stderr,"Error: no file number specified\n");

   return(1);
}

/* UI callback for 'set seed' command - initial value for random number gen */
int cli_set_seed(param)
char *param; /* remainder of command line */
{
   int size;

   if (param && (size=atoi(param))>0)
      seed=size;
   else
      fprintf(stderr,"Error: no random number seed specified\n");

   return(1);
}

/* UI callback for 'set transactions' - configure number of transactions */
int cli_set_transactions(param)
char *param; /* remainder of command line */
{
   int size;

   if (param && (size=atoi(param))>0)
      transactions=size;
   else
      fprintf(stderr,"Error: no transactions specified\n");

   return(1);
}

int parse_weight(params)
char *params;
{
   int weight=1;
   char *split;

   if (split=strrchr(params,' '))
      {
      *split='\0';
      if ((weight=atoi(split+1))<=0)
         {
         fprintf(stderr,"Error: ignoring invalid weight '%s'\n",split+1);
         weight=1;
         }
      }

   return(weight);
}

void add_location(params,weight)
char *params;
int weight;
{
   file_system *new_file_system;

   if (new_file_system=(file_system *)calloc(1,sizeof(file_system)))
      {
      strcpy(new_file_system->system.name,params);
      new_file_system->system.size=weight;

      if (file_systems)
         {
         new_file_system->prev=file_systems->prev;
         file_systems->prev->next=new_file_system;
         file_systems->prev=new_file_system;
         }
      else
         {
         new_file_system->prev=new_file_system;
         file_systems=new_file_system;
         }

      file_system_weight+=weight;
      file_system_count++;
      }
}

void delete_location(loc_name)
char *loc_name;
{
   file_system *traverse;

   for (traverse=file_systems; traverse; traverse=traverse->next)
      if (!strcmp(traverse->system.name,loc_name))
         {
         file_system_weight-=traverse->system.size;
         file_system_count--;

         if (file_systems->prev==file_systems)
            {
            free(file_systems);
            file_systems=NULL;
            }
         else
            {
            if (file_systems->prev==traverse)
               file_systems->prev=traverse->prev;

            if (traverse==file_systems)
               file_systems=file_systems->next;
            else
               traverse->prev->next=traverse->next;

            if (traverse->next)
               traverse->next->prev=traverse->prev;

            free(traverse);
            }

         break;
         }

   if (!traverse)
      fprintf(stderr,"Error: cannot find location '%s'\n",loc_name);
}

void delete_locations()
{
   file_system *next;

   while (file_systems)
      {
      next=file_systems->next;
      free(file_systems);
      file_systems=next;
      }

   file_system_weight=0;
   file_system_count=0;
}

/* UI callback for 'set location' - configure current working directory */
int cli_set_location(param)
char *param; /* remainder of command line */
{
   if (param)
      {
      switch (*param)
         {
         case '+': /* add location to list */
            add_location(param+1,parse_weight(param+1));
            break;

         case '-': /* remove location from list */
            delete_location(param+1);
            break;

         default:
            delete_locations();
            add_location(param,parse_weight(param));
         }
      }
   else
      fprintf(stderr,"Error: no directory name specified\n");

   return(1);
}

/* UI callback for 'set subdirectories' - configure number of subdirectories */
int cli_set_subdirs(param)
char *param; /* remainder of command line */
{
   int subdirs;

   if (param && (subdirs=atoi(param))>=0)
      subdirectories=subdirs;
   else
      fprintf(stderr,"Error: invalid number of subdirectories specified\n");

   return(1);
}

/* UI callback for 'set read' - configure read block size (integer) */
int cli_set_read(param)
char *param; /* remainder of command line */
{
   int size;

   if (param && (size=atoi(param))>0)
      read_block_size=size;
   else
      fprintf(stderr,"Error: no block size specified\n");

   return(1);
}

/* UI callback for 'set write' - configure write block size (integer) */
int cli_set_write(param)
char *param; /* remainder of command line */
{
   int size;

   if (param && (size=atoi(param))>0)
      write_block_size=size;
   else
      fprintf(stderr,"Error: no block size specified\n");

   return(1);
}

/* UI callback for 'set buffering' - sets buffering mode on or off
   - true = buffered I/O (default), false = raw I/O */
int cli_set_buffering(param)
char *param; /* remainder of command line */
{
   if (param && (!strcmp(param,"true") || !strcmp(param,"false")))
      buffered_io=(!strcmp(param,"true"))?1:0;
   else
      fprintf(stderr,"Error: no buffering mode (true/false) specified\n");

   return(1);
}

/* UI callback for 'set bias read' - sets probability of read vs. append */
int cli_set_bias_read(param)
char *param; /* remainder of command line */
{
   int value;

   if (param && (value=atoi(param))>=-1  && value<=10)
      bias_read=value;
   else
      fprintf(stderr,
        "Error: no bias specified (0-10 for greater chance,-1 to disable)\n");

   return(1);
}

/* UI callback for 'set bias create' - sets probability of create vs. delete */
int cli_set_bias_create(param)
char *param; /* remainder of command line */
{
   int value;

   if (param && (value=atoi(param))>=-1  && value<=10)
      bias_create=value;
   else
      fprintf(stderr,
         "Error: no bias specified (0-10 for greater chance,-1 to disable)\n");

   return(1);
}

/* UI callback for 'set report' - chooses verbose or terse report formats */
int cli_set_report(param)
char *param; /* remainder of command line */
{
   int match=0;

   if (param)
      {
      if (!strcmp(param,"verbose"))
         report=0;
      else
         if (!strcmp(param,"terse"))
            report=1;
         else
            match=-1;
      }

   if (!param || match==-1)
      fprintf(stderr,"Error: either 'verbose' or 'terse' required\n");

   return(1);
}

/* populate file source buffer with 'size' bytes of readable randomness */
char *initialize_file_source(size)
int size; /* number of bytes of junk to create */
{
   char *new_source;
   int i;

   if ((new_source=(char *)malloc(size))==NULL) /* allocate buffer */
      fprintf(stderr,"Error: failed to allocate source file of size %d\n",size);
   else
      for (i=0; i<size; i++) /* file buffer with junk */
         new_source[i]=32+RND(95);

   return(new_source);
}

/* returns differences in times -
   1 second is the minimum to avoid divide by zero errors */
time_t diff_time(t1,t0)
time_t t1;
time_t t0;
{
   return((t1-=t0)?t1:1);
}

/* prints out results from running transactions */
void verbose_report(fp,end_time,start_time,t_end_time,t_start_time,deleted)
FILE *fp;
time_t end_time,start_time,t_end_time,t_start_time; /* timers from run */
int deleted; /* files deleted back-to-back */
{
   time_t elapsed,t_elapsed;
   int interval;

   elapsed=diff_time(end_time,start_time);
   t_elapsed=diff_time(t_end_time,t_start_time);

   fprintf(fp,"Time:\n");
   fprintf(fp,"\t%d seconds total\n",elapsed);
   fprintf(fp,"\t%d seconds of transactions (%d per second)\n",t_elapsed,
      transactions/t_elapsed);

   fprintf(fp,"\nFiles:\n");
   fprintf(fp,"\t%d created (%d per second)\n",files_created,
      files_created/elapsed);

   interval=diff_time(t_start_time,start_time);
   fprintf(fp,"\t\tCreation alone: %d files (%d per second)\n",simultaneous,
      simultaneous/interval);
   fprintf(fp,"\t\tMixed with transactions: %d files (%d per second)\n",
      files_created-simultaneous,(files_created-simultaneous)/t_elapsed);
   fprintf(fp,"\t%d read (%d per second)\n",files_read,files_read/t_elapsed);
   fprintf(fp,"\t%d appended (%d per second)\n",files_appended,
      files_appended/t_elapsed);
   fprintf(fp,"\t%d deleted (%d per second)\n",files_created,
      files_created/elapsed);

   interval=diff_time(end_time,t_end_time);
   fprintf(fp,"\t\tDeletion alone: %d files (%d per second)\n",deleted,
      deleted/interval);
   fprintf(fp,"\t\tMixed with transactions: %d files (%d per second)\n",
      files_deleted-deleted,(files_deleted-deleted)/t_elapsed);

   fprintf(fp,"\nData:\n");
   fprintf(fp,"\t%s read ",scalef(bytes_read));
   fprintf(fp,"(%s per second)\n",scalef(bytes_read/(float)elapsed));
   fprintf(fp,"\t%s written ",scalef(bytes_written));
   fprintf(fp,"(%s per second)\n",scalef(bytes_written/(float)elapsed));
}

void terse_report(fp,end_time,start_time,t_end_time,t_start_time,deleted)
FILE *fp;
time_t end_time,start_time,t_end_time,t_start_time; /* timers from run */
int deleted; /* files deleted back-to-back */
{
   time_t elapsed,t_elapsed;
   int interval;

   elapsed=diff_time(end_time,start_time);
   t_elapsed=diff_time(t_end_time,t_start_time);
   interval=diff_time(t_start_time,start_time);

   fprintf(fp,"%d %d %.2f ", elapsed, t_elapsed,
      (float)transactions/t_elapsed);
   fprintf(fp, "%.2f %.2f %.2f ", (float)files_created/elapsed,
      (float)simultaneous/interval,
      (float)(files_created-simultaneous)/t_elapsed);
   fprintf(fp, "%.2f %.2f ", (float)files_read/t_elapsed,
      (float)files_appended/t_elapsed);
   fprintf(fp, "%.2f %.2f %.2f ", (float)files_created/elapsed,
      (float)deleted/interval,
      (float)(files_deleted-deleted)/t_elapsed);
   fprintf(fp, "%.2f %.2f\n", (float)bytes_read/elapsed,
      (float)bytes_written/elapsed);
}

/* returns file_table entry of unallocated file
   - if not at end of table, then return next entry
   - else search table for gaps */
int find_free_file()
{
   int i;

   if (file_allocated<simultaneous<<1 && file_table[file_allocated].size==0)
      return(file_allocated++);
   else /* search entire table for holes */
      for (i=0; i<simultaneous<<1; i++)
         if (file_table[i].size==0)
            {
            file_allocated=i;
            return(file_allocated++);
            }

   return(-1); /* return -1 only if no free files found */
}

/* write 'size' bytes to file 'fd' using unbuffered I/O */
void write_blocks(fd,size)
int fd;
int size;   /* bytes to write to file */
{
   int offset=0; /* offset into file */
   int i;

   /* write even blocks */
   for (i=size; i>=write_block_size;
      i-=write_block_size,offset+=write_block_size)
      write(fd,file_source+offset,write_block_size);

   write(fd,file_source+offset,i); /* write remainder */

   bytes_written+=size; /* update counter */
}

/* write 'size' bytes to file 'fp' using buffered I/O */
void fwrite_blocks(fp,size)
FILE *fp;
int size;   /* bytes to write to file */
{
   int offset=0; /* offset into file */
   int i;

   /* write even blocks */
   for (i=size; i>=write_block_size;
      i-=write_block_size,offset+=write_block_size)
      fwrite(file_source+offset,write_block_size,1,fp);

   fwrite(file_source+offset,i,1,fp); /* write remainder */

   bytes_written+=size; /* update counter */
}

void create_file_name(dest)
char *dest;
{
   char conversion[MAX_LINE+1];

   *dest='\0';
   if (file_system_count)
      {
      strcat(dest,
         location_index[(file_system_count==1)?0:RND(file_system_weight)]);
      strcat(dest,SEPARATOR);
      }

   if (subdirectories>1)
      {
      sprintf(conversion,"s%d%s",RND(subdirectories),SEPARATOR);
      strcat(dest,conversion);
      }

   sprintf(conversion,"%d",++files_created);
   strcat(dest,conversion);
}

/* creates new file of specified length and fills it with data */
void create_file(buffered)
int buffered; /* 1=buffered I/O (default), 0=unbuffered I/O */
{
   FILE *fp=NULL;
   int fd=-1;
   int free_file; /* file_table slot for new file */

   if ((free_file=find_free_file())!=-1) /* if file space is available */
      { /* decide on name and initial length */
      create_file_name(file_table[free_file].name);

      file_table[free_file].size=
         file_size_low+RND(file_size_high-file_size_low);

      if (buffered)
         fp=fopen(file_table[free_file].name,"w");
      else
         fd=open(file_table[free_file].name,O_RDWR|O_CREAT,0644);

      if (fp || fd!=-1)
         {
         if (buffered)
            {
            fwrite_blocks(fp,file_table[free_file].size);
            fclose(fp);
            }
         else
            {
            write_blocks(fd,file_table[free_file].size);
            close(fd);
            }
         }
      else
         fprintf(stderr,"Error: cannot open '%s' for writing\n",
            file_table[free_file].name);
      }
}

/* deletes specified file from disk and file_table */
void delete_file(number)
int number;
{
   if (file_table[number].size)
      {
      if (remove(file_table[number].name))
         fprintf(stderr,"Error: Cannot delete '%s'\n",file_table[number].name);
      else
         { /* reset entry in file_table and update counter */
         file_table[number].size=0;
         files_deleted++;
         }
      }
}

/* reads entire specified file into temporary buffer */
void read_file(number,buffered)
int number;   /* number of file to read (from file_table) */
int buffered; /* 1=buffered I/O (default), 0=unbuffered I/O */
{
   FILE *fp=NULL;
   int fd=-1;
   int i;

   if (buffered)
      fp=fopen(file_table[number].name,"r");
   else
      fd=open(file_table[number].name,O_RDONLY,0644);

   if (fp || fd!=-1)
      { /* read as many blocks as possible then read the remainder */
      if (buffered)
         {
         for (i=file_table[number].size; i>=read_block_size; i-=read_block_size)
            fread(read_buffer,read_block_size,1,fp);

         fread(read_buffer,i,1,fp);

         fclose(fp);
         }
      else
         {
         for (i=file_table[number].size; i>=read_block_size; i-=read_block_size)
            read(fd,read_buffer,read_block_size);

         read(fd,read_buffer,i);

         close(fd);
         }

      /* increment counters to record transaction */
      bytes_read+=file_table[number].size;
      files_read++;
      }
   else
      fprintf(stderr,"Error: cannot open '%s' for reading\n",
         file_table[number].name);
}

/* appends random data to a chosen file up to the maximum configured length */
void append_file(number,buffered)
int number;   /* number of file (from file_table) to append date to */
int buffered; /* 1=buffered I/O (default), 0=unbuffered I/O */
{
   FILE *fp=NULL;
   int fd=-1;
   int block; /* size of data to append */

   if (file_table[number].size<file_size_high)
      {
      if (buffered)
         fp=fopen(file_table[number].name,"a");
      else
         fd=open(file_table[number].name,O_RDWR|O_APPEND,0644);

      if ((fp || fd!=-1) && file_table[number].size<file_size_high)
         {
         block=RND(file_size_high-file_table[number].size)+1;

         if (buffered)
            {
            fwrite_blocks(fp,block);
            fclose(fp);
            }
         else
            {
            write_blocks(fd,block);
            close(fd);
            }

         file_table[number].size+=block;
         files_appended++;
         }
      else
         fprintf(stderr,"Error: cannot open '%s' for append\n",
            file_table[number].name);
      }
}

/* finds and returns the offset of a file that is in use from the file_table */
int find_used_file() /* only called after files are created */
{
   int used_file;

   while (file_table[used_file=RND(simultaneous<<1)].size==0)
      ;

   return(used_file);
}

/* reset global counters - done before each test run */
void reset_counters()
{
   files_created=0;
   files_deleted=0;
   files_read=0;
   files_appended=0;
   bytes_written=0;
   bytes_read=0;
}

/* perform the configured number of file transactions
   - a transaction consisted of either a read or append and either a
     create or delete all chosen at random */
int run_transactions(buffered)
int buffered; /* 1=buffered I/O (default), 0=unbuffered I/O */
{
   int percent; /* one tenth of the specified transactions */
   int i;

   percent=transactions/10;
   for (i=0; i<transactions; i++)
      {
      if (files_created==files_deleted)
         {
         printf("out of files!\n");
         printf("For this workload, either increase the number of files or\n");
         printf("decrease the number of transactions.\n");
         break;
         }

      if (bias_read!=-1) /* if read/append not locked out... */
         {
         if (RND(10)<bias_read) /* read file */
            read_file(find_used_file(),buffered);
         else /* append file */
            append_file(find_used_file(),buffered);
         }

      if (bias_create!=-1) /* if create/delete not locked out... */
         {
         if (RND(10)<bias_create) /* create file */
            create_file(buffered);
         else /* delete file */
            delete_file(find_used_file());
         }

      if ((i % percent)==0) /* if another tenth of the work is done...*/
         {
         putchar('.'); /* print progress indicator */
         fflush(stdout);
         }
      }

   return(transactions-i);
}

char **build_location_index(list,weight)
file_system *list;
int weight;
{
   char **index;
   int count;
   int i=0;

   if ((index=(char **)calloc(1,weight*sizeof(char *)))==NULL)
      fprintf(stderr,"Error: cannot build weighted index of locations\n");
   else
      for (; list; list=list->next)
         for (count=0; count<list->system.size; count++)
            index[i++]=list->system.name;

   return(index);
}

void create_subdirectories(dir_list,base_dir,subdirs)
file_system *dir_list;
char *base_dir;
int subdirs;
{
   char dir_name[MAX_LINE+1]; /* buffer holding subdirectory names */
   char save_dir[MAX_LINE+1];
   int i;

   if (dir_list)
      {
      for (; dir_list; dir_list=dir_list->next)
         create_subdirectories(NULL,dir_list->system.name,subdirs);
      }
   else
      {
      if (base_dir)
         sprintf(save_dir,"%s%s",base_dir,SEPARATOR);
      else
         *save_dir='\0';

      for (i=0; i<subdirs; i++)
         {
         sprintf(dir_name,"%ss%d",save_dir,i);
         MKDIR(dir_name);
         }
      }
}

void delete_subdirectories(dir_list,base_dir,subdirs)
file_system *dir_list;
char *base_dir;
int subdirs;
{
   char dir_name[MAX_LINE+1]; /* buffer holding subdirectory names */
   char save_dir[MAX_LINE+1];
   int i;

   if (dir_list)
      {
      for (; dir_list; dir_list=dir_list->next)
         delete_subdirectories(NULL,dir_list->system.name,subdirs);
      }
   else
      {
      if (base_dir)
         sprintf(save_dir,"%s%s",base_dir,SEPARATOR);
      else
         *save_dir='\0';

      for (i=0; i<subdirs; i++)
         {
         sprintf(dir_name,"%ss%d",save_dir,i);
         rmdir(dir_name);
         }
      }
}

/* CLI callback for 'run' - benchmark execution loop */
int cli_run(param) /* none */
char *param; /* unused */
{
   time_t start_time,t_start_time,t_end_time,end_time; /* elapsed timers */
   int delete_base; /* snapshot of deleted files counter */
   FILE *fp=NULL; /* file descriptor for directing output */
   int incomplete;
   int i; /* generic iterator */

   reset_counters(); /* reset counters before each run */

   sgenrand(seed); /* initialize random number generator */

   /* allocate file space and fill with junk */
   file_source=initialize_file_source(file_size_high<<1);

   /* allocate read buffer */
   read_buffer=(char *)malloc(read_block_size);

   /* allocate table of files at 2 x simultaneous files */
   file_allocated=0;
   if ((file_table=(file_entry *)calloc(simultaneous<<1,sizeof(file_entry)))==
      NULL)
      fprintf(stderr,"Error: Failed to allocate table for %d files\n",
         simultaneous<<1);

   if (file_system_count>0)
      location_index=build_location_index(file_systems,file_system_weight);

   /* create subdirectories if necessary */
   if (subdirectories>1)
      {
      printf("Creating subdirectories...");
      fflush(stdout);
      create_subdirectories(file_systems,NULL,subdirectories);
      printf("Done\n");
      }

   time(&start_time); /* store start time */

   /* create files in specified directory until simultaneous number */
   printf("Creating files...");
   fflush(stdout);
   for (i=0; i<simultaneous; i++)
      create_file(buffered_io);
   printf("Done\n");

   printf("Performing transactions");
   fflush(stdout);
   time(&t_start_time);
   incomplete=run_transactions(buffered_io);
   time(&t_end_time);
   if (!incomplete)
      printf("Done\n");

   /* delete remaining files */
   printf("Deleting files...");
   fflush(stdout);
   delete_base=files_deleted;
   for (i=0; i<simultaneous<<1; i++)
      delete_file(i);
   printf("Done\n");

   /* print end time and difference, transaction numbers */
   time(&end_time);

   /* delete previously created subdirectories */
   if (subdirectories>1)
      {
      printf("Deleting subdirectories...");
      fflush(stdout);
      delete_subdirectories(file_systems,NULL,subdirectories);
      printf("Done\n");
      }

   if (location_index)
      {
      free(location_index);
      location_index=NULL;
      }

   if (param)
      if ((fp=fopen(param,"a"))==NULL)
         fprintf(stderr,"Error: Cannot direct output to file '%s'\n",param);

   if (!fp)
      fp=stdout;

   if (!incomplete)
      reports[report](fp,end_time,start_time,t_end_time,t_start_time,
         files_deleted-delete_base);

   if (param && fp!=stdout)
      fclose(fp);

   /* free resources allocated for this run */
   free(file_table);
   free(read_buffer);
   free(file_source);

   return(1); /* return 1 unless exit requested, then return 0 */
}

/* CLI callback for 'show' - print values of configuration variables */
int cli_show(param)
char *param; /* optional: name of output file */
{
   char current_dir[MAX_LINE+1]; /* buffer containing working directory */
   file_system *traverse;
   FILE *fp=NULL; /* file descriptor for directing output */

   if (param)
      if ((fp=fopen(param,"a"))==NULL)
         fprintf(stderr,"Error: Cannot direct output to file '%s'\n",param);

   if (!fp)
      fp=stdout;

   fprintf(fp,"Current configuration is:\n");
   fprintf(fp,"The base number of files is %d\n",simultaneous);
   fprintf(fp,"Transactions: %d\n",transactions);

   if (file_size_low!=file_size_high)
      {
      fprintf(fp,"Files range between %s ",scale(file_size_low));
      fprintf(fp,"and %s in size\n",scale(file_size_high));
      }
   else
      fprintf(fp,"Files are %s in size\n",scale(file_size_low));

   fprintf(fp,"Working director%s: %s\n",(file_system_count>1)?"ies":"y",
      (file_system_count==0)?GETWD(current_dir):"");

   for (traverse=file_systems; traverse; traverse=traverse->next)
      printf("\t%s (weight=%d)\n",traverse->system.name,traverse->system.size);

   if (subdirectories>0)
      fprintf(fp,"%d subdirector%s will be used\n",subdirectories,
         (subdirectories==1)?"y":"ies");

   fprintf(fp,"Block sizes are: read=%s, ",scale(read_block_size));
   fprintf(fp,"write=%s\n",scale(write_block_size));
   fprintf(fp,"Biases are: read/append=%d, create/delete=%d\n",bias_read,
      bias_create);
   fprintf(fp,"%ssing Unix buffered file I/O\n",buffered_io?"U":"Not u");
   fprintf(fp,"Random number generator seed is %d\n",seed);

   fprintf(fp,"Report format is %s.\n",report?"terse":"verbose");

   if (param && fp!=stdout)
      fclose(fp);

   return(1); /* return 1 unless exit requested, then return 0 */
}

/* CLI callback for 'quit' - returns 0 causing UI to exit */
int cli_quit(param) /* none */
char *param; /* unused */
{
   return(0); /* return 1 unless exit requested, then return 0 */
}

/* CLI callback for 'help' - prints help strings from command_list */
int cli_help(param)
char *param; /* optional: specific command to get help for */
{
   int n=0; /* number of matching items */
   int i; /* traversal variable for command table */
   int len;

   if (param && (len=strlen(param))>0) /* if a command is specified... */
      for (i=0; command_list[i].name; i++) /* walk command table */
         if (!strncmp(command_list[i].name,param,len))
            {
            printf("%s - %s\n",command_list[i].name,command_list[i].help);
            n++;
            }

   if (!param || !n)
      for (i=0; command_list[i].name; i++) /* traverse command table */
         printf("%s - %s\n",command_list[i].name,command_list[i].help);

   return(1); /* return 1 unless exit requested, then return 0 */
}

/* read CLI line from user, translate aliases if any, return fgets status */
char *cli_read_line(buffer,size)
char *buffer; /* empty input line */
int size;
{
   char *result;

   printf("%s",PROMPT);                 /* print prompt */
   fflush(stdout);                      /* force prompt to print */
   if (result=fgets(buffer,size,stdin)) /* read line safely */
      {
      buffer[strlen(buffer)-1]='\0';    /* delete final CR */
      if (!strcmp(buffer,"?"))           /* translate aliases */
         strcpy(buffer,"help");
      if (!strcmp(buffer,"exit"))
         strcpy(buffer,"quit");
      }

   return(result);                      /* return success of fgets */
}

/* parse CLI input line */
int cli_parse_line(buffer)
char *buffer; /* line of user input */
{
   int result=1; /* default return status */
   int len; /* length of parsed command */
   int i; /* traversal variable for command table */

   if (*buffer=='!') /* check for shell escape */
      system((strlen(buffer)>1)?buffer+1:getenv("SHELL"));
   else
      {
      for (i=0; command_list[i].name; i++) /* walk command table */
         if (!strncmp(command_list[i].name,buffer,
            len=strlen(command_list[i].name)))
            { /* if command matches... */
            result=(command_list[i].func)
               (((int)strlen(buffer)>len)?buffer+len+1:NULL);
            break; /* call function and pass remainder of line as parameter */
            }

      if (!command_list[i].name) /* if no commands were called... */
         printf("Eh?\n"); /* tribute to Canadian diction */
      }

   return(result); /* return 1 unless exit requested, then return 0 */
}

/* read config file if present and process it line by line
   - if 'quit' is in file then function returns 0 */
int read_config_file(filename,buffer)
char *filename; /* file name of config file */
char *buffer;   /* temp storage for each line read from file */
{
   int result=1; /* default exit value - proceed with UI */
   FILE *fp;

   if (fp=fopen(filename,"r")) /* open config file */
      {
      printf("Reading configuration from file '%s'\n",filename);
      while (fgets(buffer,MAX_LINE,fp) && result) /* read lines until 'quit' */
         {
         buffer[strlen(buffer)-1]='\0'; /* delete final CR */
         result=cli_parse_line(buffer); /* process line as typed in */
         }

      fclose(fp);
      }

   return(result);
}


/* main function - reads config files then enters get line/parse line loop */
main(argc,argv)
int argc;
char *argv[];
{
   char buffer[MAX_LINE+1]; /* storage for input command line */

   printf("PostMark v1.5_1\n");
   printf("INITIATING ACCESS\n");
   printf("PROMPT\n");
   if (read_config_file((argc==2)?argv[1]:".pmrc",buffer))
      while (cli_read_line(buffer,MAX_LINE) && cli_parse_line(buffer))
         ;
}

/* Period parameters */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* Initializing the array with a seed */
void
sgenrand(seed)
    unsigned long seed;
{
    int i;

    for (i=0;i<N;i++) {
         mt[i] = seed & 0xffff0000;
         seed = 69069 * seed + 1;
         mt[i] |= (seed & 0xffff0000) >> 16;
         seed = 69069 * seed + 1;
    }
    mti = N;
}

/* Initialization by "sgenrand()" is an example. Theoretically,      */
/* there are 2^19937-1 possible states as an intial state.           */
/* This function allows to choose any of 2^19937-1 ones.             */
/* Essential bits in "seed_array[]" is following 19937 bits:         */
/*  (seed_array[0]&UPPER_MASK), seed_array[1], ..., seed_array[N-1]. */
/* (seed_array[0]&LOWER_MASK) is discarded.                          */
/* Theoretically,                                                    */
/*  (seed_array[0]&UPPER_MASK), seed_array[1], ..., seed_array[N-1]  */
/* can take any values except all zeros.                             */
void
lsgenrand(seed_array)
    unsigned long seed_array[];
    /* the length of seed_array[] must be at least N */
{
    int i;

    for (i=0;i<N;i++)
      mt[i] = seed_array[i];
    mti=N;
}

unsigned long
genrand()
{
    unsigned long y;
    static unsigned long mag01[2]={0x0, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if sgenrand() has not been called, */
            sgenrand(4357); /* a default initial seed is used   */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];

        mti = 0;
    }

    y = mt[mti++];
    y ^= TEMPERING_SHIFT_U(y);
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
    y ^= TEMPERING_SHIFT_L(y);

    return y;
}
