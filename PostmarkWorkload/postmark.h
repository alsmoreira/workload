/*
 * postmark.h
 *
 *  Created on: Feb 7, 2012
 *      Author: alsm
 */

#ifndef POSTMARK_H_
#define POSTMARK_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>

#ifdef _WIN32
#include <io.h>
#include <direct.h>

#define GETWD(x) getcwd(x,MAX_LINE)
#define MKDIR(x) mkdir(x)
#define SEPARATOR "\\"
#else
extern char *getwd();

#define GETWD(x) getwd(x)
#define MKDIR(x) mkdir(x,0700)
#define SEPARATOR "/"
#endif

#define MAX_LINE 255
#define MAX_FILENAME 80

#define KILOBYTE 1024
#define MEGABYTE (KILOBYTE*KILOBYTE)

#define PROMPT "pm>"

typedef struct { /* ADT for table of CLI commands */
   char *name;    /* name of command */
   int (*func)(); /* pointer to callback function */
   char *help;    /* descriptive help string */
} cmd;

extern int cli_set_size();
extern int cli_set_number();
extern int cli_set_seed();
extern int cli_set_transactions();
extern int cli_set_location();
extern int cli_set_subdirs();
extern int cli_set_read();
extern int cli_set_write();
extern int cli_set_buffering();
extern int cli_set_bias_read();
extern int cli_set_bias_create();
extern int cli_set_report();

extern int cli_run();
extern int cli_show();
extern int cli_help();
extern int cli_quit();

cmd command_list[]={ /* table of CLI commands */
   {"set size",cli_set_size,"Sets low and high bounds of files"},
   {"set number",cli_set_number,"Sets number of simultaneous files"},
   {"set seed",cli_set_seed,"Sets seed for random number generator"},
   {"set transactions",cli_set_transactions,"Sets number of transactions"},
   {"set location",cli_set_location,"Sets location of working files"},
   {"set subdirectories",cli_set_subdirs,"Sets number of subdirectories"},
   {"set read",cli_set_read,"Sets read block size"},
   {"set write",cli_set_write,"Sets write block size"},
   {"set buffering",cli_set_buffering,"Sets usage of buffered I/O"},
   {"set bias read",cli_set_bias_read,
      "Sets the chance of choosing read over append"},
   {"set bias create",cli_set_bias_create,
      "Sets the chance of choosing create over delete"},
   {"set report",cli_set_report,"Choose verbose or terse report format"},
   {"run",cli_run,"Runs one iteration of benchmark"},
   {"show",cli_show,"Displays current configuration"},
   {"help",cli_help,"Prints out available commands"},
   {"quit",cli_quit,"Exit program"},
   NULL
};

extern void verbose_report();
extern void terse_report();
void (*reports[])()={verbose_report,terse_report};

/* Counters */
int files_created;  /* number of files created */
int files_deleted;  /* number of files deleted */
int files_read;     /* number of files read */
int files_appended; /* number of files appended */
float bytes_written; /* number of bytes written to files */
float bytes_read;    /* number of bytes read from files */

/* Configurable Parameters */
int file_size_low=500;
int file_size_high=10000;       /* file size: fixed or random within range */
int simultaneous=500;           /* simultaneous files */
int seed=42;                    /* random number generator seed */
int transactions=500;           /* number of transactions */
int subdirectories=0;		    /* Number of subdirectories */
int read_block_size=512;        /* I/O block sizes */
int write_block_size=512;
int bias_read=5;                /* chance of picking read over append */
int bias_create=5;              /* chance of picking create over delete */
int buffered_io=1;              /* use C library buffered I/O */
int report=0;                   /* 0=verbose, 1=terse report format */

/* Working Storage */
char *file_source; /* pointer to buffer of random text */

typedef struct {
   char name[MAX_FILENAME+1]; /* name of individual file */
   int size;                  /* current size of file, 0 = unused file slot */
} file_entry;

file_entry *file_table; /* table of files in use */
int file_allocated;     /* pointer to last allocated slot in file_table */

typedef struct file_system_struct {
   file_entry system;
   struct file_system_struct *next,*prev;
} file_system;

file_system *file_systems; /* table of file systems/directories to use */
int file_system_weight;    /* sum of weights for all file systems */
int file_system_count;     /* number of configured file systems */
char **location_index;     /* weighted index of file systems */

char *read_buffer; /* temporary space for reading file data into */

#define RND(x) ((x>0)?(genrand() % (x)):0)
extern unsigned long genrand();
extern void sgenrand();


#endif /* POSTMARK_H_ */
