/*
 ============================================================================
 Name        : stress.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15/10/2011
 Copyright   : 
 Description : precisa do pacote coreutils mas coloquei na pasta /lib
 ============================================================================
 */

#include "stress.h"

unsigned int aux = 0;
char nomeArquivo[255] = {0};

void* stressHd(void* ptr){

	while(1){
	unsigned char val = 0xFF;
	unsigned char val2 = 0x00;
	register int i;
	unsigned int tam = 0, cont = 0;
	int livre = 0, pc = 30;
	FILE *arq;
	int limite = 1000000;

	memset(nomeArquivo,0,255);
	aux = 0;
        if((livre = show_disk("/")) == -1)
		return false;


	/*Verifica se n�o ouve erro de aloca��o de memoria no show_disk */
        if(livre == -2)
		continue;

	tam = ((pc/100.)*livre);
	if(tam > limite){
		aux = cont = tam / limite;
		tam -= cont*limite;
		limite += tam / cont;
	}
	else{
		limite = tam;
		cont = 1;
	}

	while(cont--){
		sprintf(nomeArquivo,"teste%i.data",cont);
		arq = fopen(nomeArquivo,"w+");
		if(!arq)
			continue;

		for(i=0;i<limite*1024;i++)
		{
			fwrite(&val,1,1,arq);
		}
		rewind(arq);
		for(i=0;i<limite*1024;i++)
		{
			fread(&val2,1,1,arq);
			if(val2 != val)
				continue;

		}
	}
	fclose(arq);
	while(aux--){
		sprintf(nomeArquivo,"teste%i.data",aux);
		remove(nomeArquivo);
	}

	}

	return true;
}

int show_disk(char *point)
{
	static struct mount_entry *mount_list = 0;

	unsigned char *ptr = (unsigned char*)malloc(2*sizeof(struct fs_usage));

	/* caso nao consiga alocar memoria */
	if(ptr == NULL)
		return -2;

	struct fs_usage *fsu = (struct fs_usage*)ptr;
	struct mount_entry *me = 0, *me_falso = 0;
	int achou = false;

	mount_list = read_filesystem_list(0);


	for (me = mount_list; me; me = me->me_next)
	{
		if (STREQ (me->me_mountdir, point) && !STREQ (me->me_type, "lofs"))
		{
			if(!me->me_dummy){
				achou = true;
				break;
			}
			me_falso = me;
		}
	}
	if(!achou && me_falso)
		me = me_falso;
	else
		return -1;

	if(get_fs_usage( "/" ,me->me_devname, fsu))
	{
		return -1;
	}

	free(me);

//	printf("Espaco Total: %i \n",(fsu->fsu_blocksize/1024)*(fsu->fsu_blocks));
//	printf("Espaco Livre Total: %i \n",(fsu->fsu_blocksize/1024)*fsu->fsu_bavail);

	return (fsu->fsu_blocksize/1024)*fsu->fsu_bavail;
}

/*main para testar mem�ria se quiser pode retirar o coment�rio e testar
int main(){
	volatile unsigned int enderecoBase;
	unsigned long nBytes =
			if(scanMem() == -1)
				return -1;
	if(stressDataBus() == -1)
		return -1;
	if(testeBarramentoEndereco(&enderecoBase,nBytes) == -1)
		return -1;
	return 0;
}*/

//Pega o total de memoria livre
/* Pega informa�oe sobre a memoria:
* - Livre
* - Total
*/
meminfo * freemem(){

	FILE *arq = fopen("/proc/meminfo","r");
	meminfo *mi = (meminfo*)malloc(sizeof(meminfo));

	//Verifica se abriu o arquivo
	if(arq == 0)
		return -1;

	//testa se conseguio alocar memoria */
	if(mi == NULL)
		return -1;

	char linha[500] = {0}, *aux;
	int free = 0, total = 0, usada = 0;

	fread(linha,500,1,arq);
	aux = linha + 65;
	sscanf(aux,"%i %i %i",&total,&usada,&free);
	mi->total = total;
	mi->free = free;

	fclose(arq);
	return mi;
}

//Funcao que testa barramento de Dados
unsigned char* stressDataBus(){

    unsigned char teste;
	volatile unsigned char * endereco = (volatile unsigned char *)malloc(sizeof(char));
    /*
    **	Loop p/ testar o barramento de dados inteiro.
    	00000001 --> 00000010 --> 00000100 ... at� 00000000
    */

    for (teste = 1; teste != 0; teste <<= 1)
    {
        /*  *** Escreve o "teste" no endere�o *** */
        *endereco = teste;

        /* *** Testa se conseguio gravar o "teste" no *endereco *** */

        if (*endereco != teste)
        {
            free((void*)endereco);
			return &teste;
        }
   }
    free((void*)endereco);
	return (unsigned char*)NULL;
}


//Funcao varre o espaco que esta livre na memoria gravando e lendo o valor
void* scanMem(void * ptr){
	while(1) {
		volatile unsigned char* memoriaTeste = NULL;
		unsigned long i;
		unsigned char j = 0x01;
		meminfo *mi = freemem();

		/*Verifica de ouve erro na aloca��o de memoria no freemem*/
		if(mi == NULL)
			continue;

		unsigned long  tam = mi->free;
		free(mi);

		//Subtrai o peda�o de memoria q o linux nao deixa alocar
		tam -= (unsigned long)(4.4*1024*1024);

		//verifica se nao ficou um valor incosistente
		if(tam <= 0)
			continue;

		/*Aloca toda a memoria livre*/
		memoriaTeste = malloc(sizeof(volatile unsigned char)*tam);

		/*Verifica se ouve erro na aloca��o de memoria */
		if(memoriaTeste == NULL)
			continue;

		while(j != 0){
			memset2(memoriaTeste,0,sizeof(unsigned char)*tam);
			for(i=0; i < tam; i++){
				/* Verifica se continua com zero a memoria a frente. */
				if(memoriaTeste[i] != 0);

				/* Faz uma copia na memoria de teste */
				memoriaTeste[i] = j;

				/* Verifica se a memoria est� com o valor gravado */
				if(memoriaTeste[i] != j);
			}
			j <<= 1;
		}
		free(memoriaTeste);
	}
}

/*Varre a memoria substituindo-a por um "valor" */
void memset2(unsigned char* memoriaTeste, int valor ,unsigned long tam){
	while(tam--)
		*(memoriaTeste++) = valor;
}

//Funcao que testa barramento do endereco
unsigned char* stressAddrBus()
{
	volatile unsigned char * enderecoBase = 0;

	meminfo *mi = freemem();
	//verifica se teve erro na aloca��o do freeman
	if(mi == -1)
		return -1;

	unsigned long  nBytes = mi->free;
	free(mi);

	unsigned long enderecoMask;
	unsigned long offset;
	unsigned long testOffset;

	int i;

	unsigned char pattern     = (unsigned char) 0xAAAAAAAA;
	unsigned char antipattern = (unsigned char) 0x55555555;

    /* Aumentar a eficiencia da fun��o */
    unsigned long mega = 1024*1024;
    unsigned long vet[9] = {256*mega,128*mega,64*mega,32*mega,16*mega,8*mega,4*mega,2*mega,1*mega};
    for(i=0;i<9;i++){
    	if(((signed long)nBytes - (signed long)vet[i]) > 0){
		enderecoMask = vet[i] - 1;
		break;
	}
    }


    for (offset = 1; (offset & enderecoMask) != 0; offset <<= 1)
    {
        enderecoBase[offset] = pattern;
    }

    testOffset = 0;
    enderecoBase[testOffset] = antipattern;

    for (offset = 1; (offset & enderecoMask) != 0; offset <<= 1)
    {
        if (enderecoBase[offset] != pattern)
        {
            return ((unsigned char *) &enderecoBase[offset]);
        }
    }
    enderecoBase[testOffset] = pattern;

    for (testOffset = 1; (testOffset & enderecoMask) != 0; testOffset <<= 1)
    {
        enderecoBase[testOffset] = antipattern;

                if (enderecoBase[0] != pattern)
                {
                        return ((unsigned char *) &enderecoBase[testOffset]);
                }

        for (offset = 1; (offset & enderecoMask) != 0; offset <<= 1)
        {
            if ((enderecoBase[offset] != pattern) && (offset != testOffset))
            {
                return ((unsigned char *) &enderecoBase[testOffset]);
            }
        }

        enderecoBase[testOffset] = pattern;
    }

    return (unsigned char*)NULL;
}

/* ********************************** Inicio Da Serie de Potencia **************************************** */
//Fatoria Interativo
long double fatorial(int n){
	long double x = 1;
	volatile int j;
	if(n==1 || n ==0)
		return x;
	for(j = n; j > 0 ; --j)
		x *= j;
	return x;
}
//Fatoria Recursivo
long double fatorial2(int n){
	if(n==1 || n ==0)
		return 1;
	return n * fatorial2(n-1);
}

double pow2(double x, int y){
	volatile int i;
	for(i=0; i < y; ++i)
		x *= x;
}

void* serieDePotencia(void* x){
	long double qnt = 10, soma = 0, i, n ;
	while(1){
	soma = 0;
	for (n=0; n < qnt; ++n)
		soma += pow2(sin(n),n) / fatorial(n);
	}
}

/* ************************************** Fun��o de stress *************************************** */
int stress(){

	pthread_t threadSerie, threadMemoria, threadBurn, threadHd;
	int iretSerie, iretMemoria, iretBurn, iretHd;

	iretMemoria = pthread_create(&threadMemoria, NULL, (void*)&scanMem, NULL);
	iretSerie = pthread_create(&threadSerie, NULL, (void*)&serieDePotencia, NULL);
	iretHd = pthread_create(&threadHd, NULL, (void*)&stressHd, NULL);

	sleep(120); //30 minutos

	while(aux--){
		sprintf(nomeArquivo,"teste%i.data",aux);
		remove(nomeArquivo);
	}

	return 0;
}
