/*
 ============================================================================
 Name        : stress.h
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15/10/2011
 Copyright   : 
 Description : 
 ============================================================================
 */

#ifndef STRESS_H_
#define STRESS_H_

#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/vfs.h>
#include <fsusage.h>
#include <mountlist.h>

typedef unsigned int uintmax_t;
typedef int dev_t;

#define	DIV	1024

typedef struct _meminfo{
	int free;
	int total;
} meminfo;

//usando defini��es de true e false j� que o cygwin n�o est� aceitando o padr�o da linguagem
#define true 1
#define false 0

int stress();
/* ***************************** Funcoes do teste de HD **************************************** */
void* stressHd(void* ptr)
int show_disk(char *point);

/* **************************** Funcoes do teste de Memoria ************************************* */
unsigned char* stressAddrBus(); //testa o barramento de endereco
void* scanMem(); //varre memoria
unsigned char*  stressDataBus(); //testa o barramento de dados
meminfo * freemem();
void memset2(unsigned char* memoriaTeste, int valor ,unsigned long tam); //seta a mem�ria para teste

/* ***************************** Funcoes do calculo da serie de Potencia  *********************** */
long double fatorial(int n); //Fatorial Iterativo
long double fatorial2(int n); //Fatorial Recursivo
double pow2(double x, int y); //Funcao de potencia
void* serieDePotencia(void*); //Calcula a serie de potencia

#endif /* STRESS_H_ */
