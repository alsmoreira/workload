/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15/10/2011
 Copyright   : 
 Description : 
 ============================================================================
 */

#include "stress.h"

int main(){
	if (stress() == 0)
		return 0;
	return -1;
}
