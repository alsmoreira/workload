/*
 ============================================================================
 Name        : comp_info.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15/10/2011
 Copyright   : 
 Description : Programa que retorna informa��es relativas ao hardware do com-
 	 	 	 	 putador que esteja utilizando o sistema Linux, utilizando as
 	 	 	 	 informa��es dispon�veis no /proc
 ============================================================================
 */

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <sys/vfs.h>

typedef struct _meminfo{
	int free;
    int total;
} meminfo;


int infoDisk (char *ponto);
meminfo* infoMem();
char* infoProcessador();
char* infoVideo(char*);
char* clockProcessador(char*);
int memVideo();

/*main para teste se quiser pode tirar o coment�rio e testar
int main(){
	//retorna informa��es da mem�ria
	meminfo* info = infoMem();
	char buffer[256] = {0};
	int memvideo;

	//grava as informa��es em um arquivo dinamicLog.dat
	FILE* log = fopen("dinamicLog.dat","w");
	fprintf(log,"%s\n",clockProcessador(buffer));
    fprintf(log,"%s",infoProcessador());
   	fprintf(log,"%i\n",infoDisk("/"));
	fprintf(log,"%i\n",info->total);

	//informa��es da mem�ria
	memvideo = memVideo();
	if(memVideo()==0)
		fprintf(log,"N�o dispon�vel\n");
	else
	    fprintf(log,"%i\n",memvideo);

    memset(buffer,0,256);
	fprintf(log,"%s",infoVideo(buffer));

	fclose(log);

	return 0;
}*/

//fun��o que retorna informa��es da placa de v�deo do usu�rio, por padr�o usa o /proc/pci
//mas pode ser alterado caso seja padr�o vesa (antigo) ou as pci-x (modernas)
char* infoVideo(char* buf){

    FILE *arq = fopen("/proc/pci","r");
	char linha[500] = {0}, *aux, *ptr, *ptr2;

	if(arq == 0)
		return -1;

	fgets(linha,500,arq);
	while(!(ptr = strstr(linha,"VGA compatible controller")))
		fgets(linha,500,arq);
	fclose(arq);
    aux = ptr + strlen("VGA compatible controller") + 2;
    strcpy(buf,aux);
	return buf;
}

//fun��o da mem�ria de v�deo, n�o implementada
//TODO: implementar
int memVideo(){
    return 0;
}

//fun��o que busca informa��es do processador pelo /proc
char* infoProcessador(){

	FILE *arq = fopen("/proc/cpuinfo","r");
	char linha[500] = {0}, *aux, *ptr, *ptr2;

	//Verifica se abriu o arquivo
	if(arq == 0)
		return -1;

	fgets(linha,500,arq);
	while(strncmp(linha,"model name",10))
		fgets(linha,500,arq);
	fclose(arq);

	aux = strchr(linha,':') + 2;
	ptr2 = strchr(aux,'\n');
	ptr2 = 0;
	ptr = malloc(strlen(aux) + 1);
	strcpy(ptr,aux);

	return ptr;
}

//fun��o que retorna informa��es da quantidade de mem�ria RAM do computador
//lembrando que � a real f�sica e n�o separada da compartilhada
//exemplo que acontece com algumas placas com mem�ria reservada para v�deo
meminfo* infoMem(){
	FILE *arq = fopen("/proc/meminfo","r");
	meminfo *mi = (meminfo*)malloc(sizeof(meminfo));

	//Verifica se abriu o arquivo
	if(arq == 0)
		return -1;

	//testa se conseguiu alocar memoria */
	if(mi == NULL)
		return -1;

	char linha[500] = {0}, *aux;
	int free = 0, total = 0, usada = 0;

	fread(linha,500,1,arq);
	aux = linha + 65;
	sscanf(aux,"%i %i %i",&total,&usada,&free);
	mi->total = total;
	mi->free = free;

	fclose(arq);
	return mi;
}

//informa��es relacionadas a quantidade de blocos dispon�veis no disco
//TODO: refinar as informa��es
int infoDisk (char *ponto)
{
	struct statfs stat;

	int val = statfs(ponto, &stat);
	if(val == -1)
		return -1;
	return (stat.f_bsize/1024)*stat.f_blocks;
}

//fun��o para o clock do processador
char* clockProcessador(char* buffer){

    FILE *arq = fopen("/proc/cpuinfo","r");
	char linha[500] = {0}, *aux, *ptr;
	float clock = 0;

	//Verifica se abriu o arquivo
	if(arq == 0)
		return -1;

	fgets(linha,500,arq);
	while(strncmp(linha,"cpu MHz",7))
		fgets(linha,500,arq);
	fclose(arq);

	aux = strchr(linha,':') + 2;
	for(ptr = aux;isdigit(*ptr) || (*ptr) == '.'; ++ptr);
	*ptr = 0;
	strcpy(buffer,aux);
	return buffer;
}

