/*
 ============================================================================
 Name        : knapsack.cpp
 Author      : Anderson Moreira
 E-mail      : anderson.moreira@recife.ifpe.edu.br
 Version     : 10/04/2012
 Copyright   : 
 Description :
 	 	 	 entradas:
 	 	 	   num_item - numero de itens
 	 	 	   valor_item - vetor com o valor dos itens
 	 	 	   peso_item - vetor com o peso dos itens
 	 	 	   peso_total - capacidade da mochila
 	 	 	   solucao - aponta para um bloco de mem�ria v�lido para alocar a melhor sele��o
 	 	 	 saidas:
 	 	 	   ideal - melhor valor encontrado para a mochila
 ============================================================================
 */

#include "knapsack.h"

double resolver(int num_item, double *valor_item, double *peso_item, double peso_total, int *solucao)
{
	int i,j; //vari�veis de loop
	double *tabela_solucoes; //tabela de solu��es tabela_solucoes
	double w_passo = peso_total / ESPACOS;

	//imprime os passos
	printf("Passos do Peso Total eh %g, %d itens\n", w_passo, num_item);

	int cf = ESPACOS+1;  //comprimento da fila
	tabela_solucoes = (double *)malloc(sizeof(double)*(num_item+1)*cf);

	int *prev_solucao = (int *)malloc(sizeof(int)*num_item*cf);
	int *cur_solucao = (int *)malloc(sizeof(int)*num_item*cf);

	//tomar cuidade com o primeiro ser zero
	for (i=0; i < cf; i++)
		tabela_solucoes[i] = 0;
	memset(cur_solucao, 0, sizeof(int)*num_item*cf);

	//come�a incrementar o n�mero de itens considerado
	for (i=1; i < (num_item+1); i++) {
		tabela_solucoes[i*cf] = 0; //seta a entrada (i,0) = 0
		//para i itens, qual � o melhor peso total se os pesos dos itens aumentarem
		for (j=1; j < cf; j++) {
			double curw = w_passo*j;
			if (peso_item[i-1] <= curw) {
				int temp = (int)floor((curw - peso_item[i-1])/w_passo);
				//verifica para ver se novos itens podem ser alocados na solu��o
				if ((valor_item[i-1] + tabela_solucoes[(i-1)*cf + temp]) > tabela_solucoes[(i-1)*cf + j]) {
					tabela_solucoes[i*cf + j] = valor_item[i-1] + tabela_solucoes[(i-1)*cf + temp];
					memcpy(&cur_solucao[j*num_item], &prev_solucao[temp*num_item], sizeof(int)*num_item);
					cur_solucao[j*num_item + i-1] = 1;
				}
				else {
					tabela_solucoes[i*cf + j] = tabela_solucoes[(i-1)*cf + j];
					memcpy(&cur_solucao[j*num_item], &prev_solucao[j*num_item], sizeof(int)*num_item);
				}
			}
			else { //novo item i n�o cabe
				tabela_solucoes[i*cf + j] = tabela_solucoes[(i-1)*cf + j];
				memcpy(&cur_solucao[j*num_item], &prev_solucao[j*num_item], sizeof(int)*num_item);
			}
		}
		memcpy(prev_solucao, cur_solucao, sizeof(int)*num_item*cf); //atualiza solu��o pr�via
	}

	//imprime
	for (i=0;i<(num_item+1);i++) {
	   for (j=1; j<cf; j++) {
	      printf("%g ", tabela_solucoes[i*cf + j]);
	   }
	   printf("\n");
	}

	memcpy(solucao, &cur_solucao[(cf-1)*num_item], sizeof(int)*num_item);
	double ideal = tabela_solucoes[(num_item+1)*cf - 1];

	free(tabela_solucoes);
	free(prev_solucao);
	free(cur_solucao);

	return ideal;
}
