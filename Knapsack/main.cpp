/*
 ============================================================================
 Name        : main.cpp
 Author      : Anderson Moreira
 E-mail      : anderson.moreira@recife.ifpe.edu.br
 Version     : 10/04/2012
 Copyright   : 
 Description : 
 ============================================================================
 */

#include "knapsack.h"

int main(int argc, char **argv){

	int numero_de_itens = 3;
	double valores[3] = {4.5, 1.4, 5.0};
	double pesos[3] = {10.0, 5.0, 10.0};
	double peso_total = 16.0;
	int *resposta = (int *)malloc(sizeof(int)*numero_de_itens);

	printf("Solucao ideal: %lf",resolver(numero_de_itens, valores, pesos, peso_total, resposta));
}
