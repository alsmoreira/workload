/*
 ============================================================================
 Name        : knapsack.h
 Author      : Anderson Moreira
 E-mail      : anderson.moreira@recife.ifpe.edu.br
 Version     : 10/04/2012
 Copyright   : 
 Description : solucao para o problema de knapsack NP-completos de Richard
 	 	 	 	 Karp, exposto em 1972.
 ============================================================================
 */

#ifndef KNAPSACK_H_
#define KNAPSACK_H_

#include <string.h> //necess�rio para string
#include <iostream>
#include <cstdlib> //necess�rio para rand() - rand�mico
#include <cstdio>
#include <math.h>

#define ESPACOS  10

using namespace std;

//fun��o de solu��o do problema da mochila
double resolver(int num_item, double *valor_item, double *peso_item, double peso_total, int *solucao);

#endif /* KNAPSACK_H_ */
