#include "stress.h"

FILE *arq;
unsigned int aux = 0;
char nomeArquivo[255] = {0};
PROCESS_INFORMATION pid;
MEMORYSTATUS stat;; // Pega informa�oe sobre a memoria:

/* teste de HD */

void testaHd(void * ptr){

	while(1){
		ULARGE_INTEGER total;
		GetDiskFreeSpaceEx("C:",NULL, NULL,&total);

		unsigned char val = 0xFF;
		unsigned char val2 = 0x00;
		register int i;
		unsigned int tam = 0,  cont = 0;
		int pc =40;

		int limite = 1000000;
		__int64 livre = (__int64)total.QuadPart/1024;

		tam = (unsigned int)((pc/100.)*livre);
		if(tam > limite){
			aux = cont = tam / limite;
			tam -= cont*limite;
			limite += tam / cont;
		}//fim do if
		else{
			limite = tam;
			aux = cont = 1;
		}//fim do else

		while(cont--){
			sprintf(nomeArquivo,"teste%i.data",cont);
			arq = fopen(nomeArquivo,"w+");
			if(!arq);
			for(i=0;i<limite*1024;i++)
			{
				fwrite(&val,1,1,arq);
			}//fim do for
			rewind(arq);
			for(i=0;i<limite*1024;i++)
			{
				fread(&val2,1,1,arq);
				if(val2 != val);
			}//fim do for

			fclose(arq);
		}//fim do while

		while(aux--){
			sprintf(nomeArquivo,"del teste%i.data",aux);
			system(nomeArquivo);
			remove(nomeArquivo);
		}//fim do while
	}//fim do while
	return;
} //fim da fun��o testaHd

/* teste de mem�ria */

void VarreMemoria(void* ptr){
	
	while(1){
		unsigned char* memoriaTeste = NULL;
		unsigned long i;
		unsigned char j = 0x01;
		unsigned long  tam = stat.dwAvailPhys;
		
		tam -= 4.4*1024*1024;
		if(tam <= 0)
			continue;
		
		/*Aloca toda a memoria livre*/

		memoriaTeste = (unsigned char*)malloc(sizeof(volatile unsigned char)*stat.dwAvailPhys);

		/*Verifica se ouve erro na aloca��o de memoria */
		if(memoriaTeste == 0)
			continue;
			
		while(j != 0){ 
			memset2(memoriaTeste,0,sizeof(unsigned char)*stat.dwAvailPhys);
			for(i=0; i < tam; i++){
				/* Verifica se continua com zero a memoria a frente. */
				if(memoriaTeste[i] != 0);
				/* Faz uma copia na memoria de teste */
				memoriaTeste[i] = j; 
				/* Verifica se a memoria est� com o valor gravado */
				if(memoriaTeste[i] != j);
			}//fim do for
			j <<= 1;
		}//fim do while
		delete [] memoriaTeste;
	}//fim do while
}//fim da fun��o varreMemoria

/*Varre a mem�ria substituindo-a por uma vari�vel "valor" */
void memset2(volatile unsigned char* memoriaTeste, int valor ,unsigned long tam){
	while(tam--)
		*(memoriaTeste++) = valor;
} //fim da fun��o menset2

/*fun��o de fatorial interativo */

long double fatorial(int n){
	long double x = 1;
	volatile int j;
	if(n==1 || n ==0)
		return x;
	for(j = n; j > 0 ; --j)
		x *= j;
	return x;
} //fim da fun��o fatorial

/*fun��o de s�rie de pot~encia */
void serieDePotencia(void* x){
	long double qnt = 10, soma = 0, n ;

	while(1){
		soma = 0;
		for (n=0; n < qnt; ++n)
			soma += pow(sin(n),(double)n) / fatorial(n);
	}//fim do while
}//fim da fun��o serieDePotencia

/* TO-DO - rever essa fun��o pois deixou meu comp louco
void burn(void * x){
	STARTUPINFO si;
    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pid, sizeof(pid) );
	CreateProcess( "burn.exe",0,0,0,FALSE,0,NULL,NULL,&si,&pid);
	return;
}
*/

/* fun��o de stress */
int stress(unsigned long tempo){
	
	printf("\nInicio de teste de memoria, CPU, HDD\n");
	_beginthread(VarreMemoria,0,0);
	_beginthread(serieDePotencia,0,0);
	_beginthread(testaHd,0,0);

	Sleep(tempo*1000);
	return 0;
}

/*executa stress */
void run (){
	GlobalMemoryStatus (&stat);
	stress(180);
	TerminateProcess (pid.hProcess,0);
}
